package com.saurshaz.poc.jee.delegate;

import com.saurshaz.poc.jee.models.User;
import com.saurshaz.poc.jee.service.LoginService;
import com.saurshaz.poc.jee.service.LoginServiceImpl;

public class LoginDelegateImpl implements LoginDelegate {

	@Override
	public boolean authenticateUser(User user) {

		LoginService loginService = new LoginServiceImpl();
		return loginService.authenticateUser(user);
	}

}
