package com.saurshaz.poc.jee.delegate;

import com.saurshaz.poc.jee.models.User;

public interface LoginDelegate {
	
	public boolean authenticateUser(User user);
}
