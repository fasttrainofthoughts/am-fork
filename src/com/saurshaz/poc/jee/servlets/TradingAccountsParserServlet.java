package com.saurshaz.poc.jee.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TradingAccountsParserServlet extends BaseServlet {

	private static final long serialVersionUID = -190321426679069804L;

	// returns JSON string
	private String getTradingAccountInfo(String term) {
		
		System.out.println("Term is "+ term);
		
		StringBuffer jsonString = new StringBuffer("[");
		jsonString.append("{\"item\":\"text 1\",\"value\":\" 12312343 \"},");
		jsonString.append("{\"item\":\"text 1\",\"value\":\" 123752343 \"},");
		jsonString.append("{\"item\":\"text 1\",\"value\":\" 12312343 \"},");
		jsonString.append("{\"item\":\"text 1\",\"value\":\" 12389343 \"},");
		jsonString.append("{\"item\":\"text 1\",\"value\":\" 12397343 \"},");
		jsonString.append("{\"item\":\"text 1\",\"value\":\" 12311343 \"},");
		jsonString.append("{\"item\":\"text 1\",\"value\":\" 12312763 \"},");
		jsonString.append("{\"item\":\"text 2\",\"value\":\" 12313233 \"}");
		
		jsonString.append("]");
		System.out.println(jsonString.toString());
		return jsonString.toString();
	}

	@Override
	public void doProcess(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("application/json");
		PrintWriter out = res.getWriter();
		String str = getTradingAccountInfo(req.getParameter("term"));
		out.write(str); 
	}
	

}
