package com.saurshaz.poc.jee.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends BaseServlet{

	
	public void doProcess(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		System.out.println("start processing for home page -- in the servlet");
		
		req.getRequestDispatcher("/WEB-INF/pages/home.jsp").forward(req, res);
		
		System.out.println("end processing for home page -- in the servlet");
	}
	
}
