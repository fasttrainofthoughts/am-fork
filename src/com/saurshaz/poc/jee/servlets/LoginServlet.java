package com.saurshaz.poc.jee.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.saurshaz.poc.jee.delegate.LoginDelegate;
import com.saurshaz.poc.jee.delegate.LoginDelegateImpl;
import com.saurshaz.poc.jee.models.User;

public class LoginServlet extends BaseServlet {

	private AtomicInteger incrementCnt = new AtomicInteger(1);

	@Override
	public void doProcess(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		System.out.println(" for the authentication process");

		LoginDelegate loginDelegate = new LoginDelegateImpl();

		Map<String, User> userMap = null;
		if (userMap == null) {
			userMap = new HashMap<String, User>();
			generateUser(req,userMap);
			req.getSession().setAttribute("userMap", userMap);
			res.sendRedirect("/am/secured/home");
		} else {
			User user = userMap.get(req.getSession().getId());
			if (user != null) {
				if (loginDelegate.authenticateUser(user)) {
					res.sendRedirect("/am/securd/home");
				} else {
					generateUser(req, userMap);
					req.getSession().setAttribute("userMap", userMap);
					res.sendRedirect("/am/secured/home");
				}
			}
		}
		

	}

	private void generateUser(HttpServletRequest req, Map<String, User> userMap) {
		User user;
		// generate new User
		user = new User(req.getSession().getId(), incrementCnt.addAndGet(1));
		userMap.put(req.getSession().getId(), user);
	}

}
