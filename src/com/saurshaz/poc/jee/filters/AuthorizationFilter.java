package com.saurshaz.poc.jee.filters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.saurshaz.poc.jee.models.User;

public class AuthorizationFilter implements Filter {

	@Override
	public void destroy() {
		System.out.println("auth filter destroyed");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain filterChain) throws IOException, ServletException {
		System.out.println("auth filter called");

		HttpSession session = ((HttpServletRequest) req).getSession(false);
		User currentUser = null;
		Map<String, User> userMap = null;
		if (session == null || session.getAttribute("userMap") == null) {
			userMap = new HashMap<String, User>();

			// user not logged in , redirect to Login page
			req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(req,
					res);
			
		} else {
			userMap = (Map<String, User>) session.getAttribute("userMap");
			currentUser = (User) userMap.get(session.getId());

			if (currentUser == null) {
				// user not logged in , redirect to Login page
				req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(
						req, res);
			} else {
				// User already logged in, continue to the accessed uri
				if(currentUser.isNewUser()){
					req.setAttribute("isError", "true");
					req.setAttribute("user", currentUser);
					currentUser.setNewUser(false);
					userMap.put(session.getId(), currentUser);
				}
				filterChain.doFilter(req, res);
			}
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println(" auth filter initialized");
	}

}