package com.saurshaz.poc.jee.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.saurshaz.poc.jee.models.User;

public class LoginServiceImpl implements LoginService {

	public boolean authenticateUser(User user) {
		boolean result = false;
		// if(user.getId().split(""))

		result = checkUser(user);

		return result;
	}

	private boolean checkUser(User user) {

		String regexId = "user@[\\d]";
		Pattern patternId = Pattern.compile(regexId, Pattern.CASE_INSENSITIVE);

		Matcher mId = patternId.matcher(user.getUserid());
		if (mId.find()) {
			// id is present
			String regexPwd = "password@[\\d]"; // match only patterns
			Pattern patternPassword = Pattern.compile(regexPwd);
			Matcher mPwd = patternPassword.matcher(user.getPassword());
			if (mPwd.find()) {
				// correct credentials
				return true;
			} else {
				// incorrect password
				return false;
			}
		} else {
			// incorrect id
			return false;
		}
	}
}
