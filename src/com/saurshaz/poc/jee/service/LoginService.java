package com.saurshaz.poc.jee.service;

import com.saurshaz.poc.jee.models.User;

public interface LoginService {
	public boolean authenticateUser(User user) ;
}
