package com.saurshaz.poc.jee.models;

public class User extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6872316746223619307L;
	private String id;
	private String userid;
	private boolean isNewUser;

	public boolean isNewUser() {
		return isNewUser;
	}

	public User(String id, int cnt) {
		super();
		this.id = id;
		this.userid = "user" + cnt;
		this.password = "password" + cnt;
		this.isNewUser = true;
	}

	public void setNewUser(boolean isNewUser) {
		this.isNewUser = isNewUser;
	}

	public User(String id) {
		super();
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	private String password;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			System.out.println(" line 44 " + obj != null
					&& obj.getClass().equals(User.class));
			return ((User) obj).getId().equals(this.getId());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

}
